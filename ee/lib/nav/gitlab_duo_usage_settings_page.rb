# frozen_string_literal: true

module Nav
  module GitlabDuoUsageSettingsPage
    include ::GitlabSubscriptions::SubscriptionHelper
    include ::GitlabSubscriptions::CodeSuggestionsHelper

    def show_gitlab_duo_usage_menu_item?(group)
      group.usage_quotas_enabled? &&
        show_gitlab_duo_usage_app?(group)
    end

    def show_gitlab_duo_usage_app?(group)
      gitlab_com_subscription? &&
        (
          !group.has_free_or_no_subscription? ||
          # group has Duo Pro add-on trial on a Free tier
          group.subscription_add_on_purchases.active.for_gitlab_duo_pro.any?
        ) &&
        # Guard this feature for EE users only https://docs.gitlab.com/ee/development/ee_features.html#guard-your-ee-feature
        # This is to prevent it showing up in CE and free EE
        License.feature_available?(:code_suggestions)
    end
  end
end
